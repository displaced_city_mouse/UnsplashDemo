package com.jlf.unsplashdemo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Photo {

    private String id;

    private Integer width;
    private Integer height;

    private String description;

    private List<Url> urls;
}
