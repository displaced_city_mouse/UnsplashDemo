package com.jlf.unsplashdemo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Url {
    private String raw;
    private String full;
    private String regular;
    private String small;
    private String thumb;
}
